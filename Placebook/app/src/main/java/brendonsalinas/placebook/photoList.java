package brendonsalinas.placebook;


import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

public class photoList extends ArrayAdapter<String>{

    private final Activity context;
    private final ArrayList<String> place;
    private final ArrayList<String> description;
    private final ArrayList<String> photo;
    private final ArrayList<Long> id;

    public photoList(Activity context, ArrayList<String> place, ArrayList<String> description
    , ArrayList<String> photo, ArrayList<Long> id) {
        super(context, R.layout.row_layout,place);
        this.context = context;
        this.place = place;
        this.description = description;
        this.photo = photo;
        this.id = id;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View list = inflater.inflate(R.layout.row_layout, null, true);


        ImageView imageView = (ImageView) list.findViewById(R.id.row_image_view);
        if (photo.get(position) != null) {
            File picture = new File(photo.get(position));
            if (picture.exists()) {
                imageView.setImageBitmap(getBitmap(picture,100,100));
            }
        }
        TextView txtPlace = (TextView) list.findViewById(R.id.row_txtPlace);
        txtPlace.setText(place.get(position));

        TextView txtDescription = (TextView) list.findViewById(R.id.row_txtPlaceDesc);
        txtDescription.setText(description.get(position));

        return list;
    }

    public static Bitmap getBitmap(File pic, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pic.getAbsolutePath(), options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pic.getAbsolutePath(), options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
