package brendonsalinas.placebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.support.v7.app.ActionBarActivity;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;

public class HistoryActivity extends ActionBarActivity implements ActionMode.Callback{
    private ArrayList< PlacebookEntry > mPlacebookEntries ;
    private ListView mListView;
    protected Object mActionMode;
    public int selectedItem = -1;
    private ArrayList<String> places;
    private ArrayList<String> desc;
    private ArrayList<String> pics;
    private ArrayList<Long> id;
    private photoList myadapter;
    public Intent intent;
    private boolean searchPlace = false;

    private EditText newSearch;
    private Button searchNew;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super . onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        places = new ArrayList<>();
        desc = new ArrayList<>();
        pics = new ArrayList<>();
        id = new ArrayList<>();

        intent = getIntent ();
        mPlacebookEntries = intent . getParcelableArrayListExtra ( MainActivity.VIEW_ALL_KEY );

        for (PlacebookEntry place: mPlacebookEntries) {
            id.add(place.getId());
            places.add(place.getName());
            desc.add(place.getDescription());
            pics.add(place.getPhotoPath());
        }

        mListView = (ListView) findViewById(R.id.listview);

        photoList adapter = new photoList(HistoryActivity.this,places,desc,pics,id);
        myadapter = adapter;
        mListView.setAdapter(adapter);

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (mActionMode != null)
                    return false;
                selectedItem = position;
                mActionMode = HistoryActivity.this.startActionMode(HistoryActivity.this);
                view.setSelected(true);
                return true;
            }
        });
    }
    @Override
    protected void onDestroy () {
        Intent intent = new Intent () ;
        intent . putParcelableArrayListExtra ( MainActivity.VIEW_ALL_KEY , mPlacebookEntries);
        setResult(Activity.RESULT_OK, intent) ;
        super . onDestroy() ;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.rowselection, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            //delete place
            case R.id.action_delete_place:
                //Delete Item
                places.remove(selectedItem);
                desc.remove(selectedItem);
                pics.remove(selectedItem);
                myadapter.notifyDataSetChanged();
                mPlacebookEntries.remove(selectedItem);
                Toast.makeText(getBaseContext(),"Deleted Place",Toast.LENGTH_SHORT).show();
                mode.finish();
                return true;
            //edit place
            case R.id.action_edit_place:
                //Edit Item
                intent.putExtra("change",selectedItem);
                intent.putParcelableArrayListExtra(MainActivity.VIEW_ALL_KEY,mPlacebookEntries);
                setResult(MainActivity.REQUEST_CHANGE, intent);
                finish();
                return true;
            default: return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //code to check how back arrow works
        if(item.getItemId() != R.id.action_search && item.getItemId() != R.id.action_settings && !searchPlace){
            intent.putParcelableArrayListExtra(MainActivity.VIEW_ALL_KEY, mPlacebookEntries);
            setResult(MainActivity.REQUEST_VIEW_ALL, intent);
            super.onBackPressed();
            finish();
            return true;
        }
        //if back arrow was pressed during a search
        else if(item.getItemId() != R.id.action_search && item.getItemId() != R.id.action_settings && searchPlace){
            setContentView(R.layout.activity_history);
            searchPlace = false;

            mListView = (ListView) findViewById(R.id.listview);

            photoList adapter = new photoList(HistoryActivity.this,places,desc,pics,id);
            myadapter = adapter;
            mListView.setAdapter(adapter);

            mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    if (mActionMode != null)
                        return false;
                    selectedItem = position;
                    mActionMode = HistoryActivity.this.startActionMode(HistoryActivity.this);
                    view.setSelected(true);
                    return true;
                }
            });
        }

        switch (item.getItemId()) {

            case R.id.action_search:
                searchPlace = true;
                setContentView(R.layout.activity_search);
                newSearch = (EditText) findViewById(R.id.searchQuery);

                searchNew = (Button) findViewById(R.id.btnSearch);
                searchNew.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        places = new ArrayList<>();
                        desc = new ArrayList<>();
                        pics = new ArrayList<>();
                        id = new ArrayList<>();

                        if (newSearch.getText() == null) {
                            newSearch.setText("");
                        }
                        String fromText = newSearch.getText().toString();

                        //Looks to see if the entered text is anywhere in the place title
                        for (PlacebookEntry place : mPlacebookEntries) {
                            if (place.getName().contains(fromText)) {
                                id.add(place.getId());
                                places.add(place.getName());
                                desc.add(place.getDescription());
                                pics.add(place.getPhotoPath());
                            }
                        }

                        newSearch.setText("");

                        mListView = (ListView) findViewById(R.id.searchView);

                        photoList adapter = new photoList(HistoryActivity.this, places, desc, pics, id);
                        myadapter = adapter;
                        mListView.setAdapter(adapter);

                        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                            @Override
                            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                                if (mActionMode != null)
                                    return false;
                                selectedItem = position;
                                mActionMode = HistoryActivity.this.startActionMode(HistoryActivity.this);
                                view.setSelected(true);
                                return true;
                            }
                        });
                    }
                });
                return true;
            default:return super.onOptionsItemSelected(item);
        }
    }



    @Override
    public void onDestroyActionMode(ActionMode mode) {
        mActionMode = null;
        selectedItem = -1;
    }

    @Override
    public void onBackPressed() {
        if (!searchPlace) {
            intent.putParcelableArrayListExtra(MainActivity.VIEW_ALL_KEY, mPlacebookEntries);
            setResult(MainActivity.REQUEST_VIEW_ALL, intent);
            super.onBackPressed();
            finish();
        }
        else {
            setContentView(R.layout.activity_history);
            searchPlace = false;

            //Each time you go back all of the places get reloaded
            places = new ArrayList<>();
            desc = new ArrayList<>();
            pics = new ArrayList<>();
            id = new ArrayList<>();

            for (PlacebookEntry place: mPlacebookEntries) {
                id.add(place.getId());
                places.add(place.getName());
                desc.add(place.getDescription());
                pics.add(place.getPhotoPath());
            }

            mListView = (ListView) findViewById(R.id.listview);

            photoList adapter = new photoList(HistoryActivity.this,places,desc,pics,id);
            myadapter = adapter;
            mListView.setAdapter(adapter);

            mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    if (mActionMode != null)
                        return false;
                    selectedItem = position;
                    mActionMode = HistoryActivity.this.startActionMode(HistoryActivity.this);
                    view.setSelected(true);
                    return true;
                }
            });
        }
    }
}
