package brendonsalinas.placebook;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcel;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.ErrorDialogFragment;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class MainActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener{
    public static final String VIEW_ALL_KEY = "brendonsalinas.placebook.EXTRA_VIEW_ALL";

    private static final int REQUEST_IMAGE_CAPTURE = 1001;
    private static final int REQUEST_SPEECH_INPUT = 1002;
    private static final int REQUEST_PLACE_PICKER = 1003;
    public static final int REQUEST_VIEW_ALL = 1005;
    public static final int REQUEST_CHANGE = 1006;
    private static long items = 0;

    private ArrayList<PlacebookEntry > mPlaceBookEntries;

    String mCurrentPhotoPath = null;

    private EditText placeContent;
    private EditText descriptionContent;

    private Location location = null;
    private TextView LatitudeText;
    private TextView LongitudeText;
    private TextView AltitudeText;

    private ImageButton btnSnapShot;
    private ImageButton btnSpeech;
    private ImageButton btnPlacePicker;
    private ImageButton btnGPS;

    private int change = -1;

    private File photoFile = null;

    private LocationRequest newLocation;

    private GoogleApiClient mGoogleApiClient;

    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1004;
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initGoogleApi();

        placeContent = (EditText) findViewById(R.id.txtPlaceContent);
        descriptionContent = (EditText) findViewById(R.id.edit_place_desc);

        //Creates the buttons and the button knows what to do when it is clicked
        btnSnapShot = (ImageButton) findViewById(R.id.button_snapshot);
        btnSnapShot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        btnSpeech = (ImageButton) findViewById(R.id.button_speak);
        btnSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchSpeechInputIntent();
            }
        });

        btnPlacePicker = (ImageButton) findViewById(R.id.button_place_picker);
        btnPlacePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchPlacePicker();
            }
        });

        btnGPS = (ImageButton) findViewById(R.id.button_location);
        btnGPS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateLocation();
            }
        });

        LatitudeText = (TextView) findViewById(R.id.txtGpsLatitudeContent);
        LongitudeText = (TextView) findViewById(R.id.txtGpsLongitudeContent);
        AltitudeText = (TextView) findViewById(R.id.txtGpsAltitudeContent);

        mPlaceBookEntries = new ArrayList<>();
    }

    private void initGoogleApi() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        //Getting a new location every 1 to 10 seconds
        newLocation = LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY).setInterval(5*1000).setFastestInterval(1000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case R.id.action_settings: /** Show setting **/
                return true;
            case R.id.action_new_place: /** code to add a new place **/;
                if (change == -1) {
                    //Make sure that all of the required fields are filled
                    //Check if a picture has been taken
                    if (mCurrentPhotoPath != null) {
                        //check if the place has been picked
                        if (!LongitudeText.getText().toString().matches("")) {
                            //Check if the Name has been added. No description is needed
                            if (!placeContent.getText().toString().matches("")) {
                                //Save the new Place into the arraylist of places to be used by the history activity
                                newPlace();
                                Toast.makeText(getBaseContext(), "Place Added", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getBaseContext(),"No Place", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getBaseContext(),"No location",Toast.LENGTH_LONG).show();
                        }
                    } else{
                        Toast.makeText(getBaseContext(),"No picture",Toast.LENGTH_LONG).show();
                    }
                } else {
                    changePlace(change);
                    Toast.makeText(getBaseContext(),"Place Changed",Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_view_all: /**code to show all places**/;
                dispatchViewAllPlaces();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    private File createImageFile() throws IOException{
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;

    }

    //Called when the snapShot button is clicked by the user
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Make sure there is a camera activity that will handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            //Try to make a place to save the picture
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
        else {
            Toast.makeText(getBaseContext(),"No Camera is installed",Toast.LENGTH_LONG).show();
        }
    }

    //Called when the user hits the speech-to-text button
    void dispatchSpeechInputIntent() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(
                RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,getString(R.string.speech_prompt));

        try {
            startActivityForResult(intent, REQUEST_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            //Handle Exception
            Toast.makeText(getBaseContext(),"Not supported", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //check the image size
        if (resultCode == RESULT_OK && requestCode == REQUEST_IMAGE_CAPTURE) {
            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);

            if(bitmap.getHeight() > 8000 || bitmap.getWidth()>8000){
                Toast.makeText(getBaseContext(),"Image is too big. Make it smaller.", Toast.LENGTH_SHORT).show();
                mCurrentPhotoPath = null;
            }
        }


        if (resultCode == RESULT_OK && requestCode == REQUEST_SPEECH_INPUT) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
        }

        if (resultCode == RESULT_OK && requestCode == REQUEST_PLACE_PICKER && data != null) {
            Place place = PlacePicker.getPlace(data,this);
            //Set place name text view to place.getName()
            placeContent.setText(place.getName());
        }

        //used to update the list of placebook entries
        if(resultCode == REQUEST_VIEW_ALL && requestCode == REQUEST_VIEW_ALL){
            mPlaceBookEntries = data.getParcelableArrayListExtra(VIEW_ALL_KEY);
        }
        if (resultCode == REQUEST_CHANGE && requestCode == REQUEST_VIEW_ALL) {
            change = data.getIntExtra("change",-1);
            mPlaceBookEntries = data.getParcelableArrayListExtra(VIEW_ALL_KEY);
            placeContent.setText(mPlaceBookEntries.get(change).getName());
            descriptionContent.setText(mPlaceBookEntries.get(change).getDescription());
            LatitudeText.setText(mPlaceBookEntries.get(change).getlatitude());
            LongitudeText.setText(mPlaceBookEntries.get(change).getLongitude());
            AltitudeText.setText(mPlaceBookEntries.get(change).getAltitude());
            mCurrentPhotoPath = mPlaceBookEntries.get(change).getPhotoPath();
        }
    }


    //Called when the user hits place picker
    private void launchPlacePicker() {
        updateLocation();
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        Context context = getApplicationContext();

        try {
            startActivityForResult(builder.build(context),REQUEST_PLACE_PICKER);
        } catch (GooglePlayServicesRepairableException e ) {
            //Error
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            //Error
        }

    }

    @Override
    public void onConnected(Bundle bundle) {
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,newLocation, this);
        }
    }

    private void updateLocation() {
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            LatitudeText.setText(String.valueOf(location.getLatitude()));
            LongitudeText.setText(String.valueOf(location.getLongitude()));
            AltitudeText.setText(String.valueOf(location.getAltitude()));
        }
        else {
            Toast.makeText(getBaseContext(),"Please try to get location again",Toast.LENGTH_SHORT).show();
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, newLocation, this);
            location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            mResolvingError = true;
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    private void dispatchViewAllPlaces() {
        Intent intent = new Intent(this, HistoryActivity.class);
        intent.putParcelableArrayListExtra(VIEW_ALL_KEY, mPlaceBookEntries);
        try {
            startActivityForResult(intent , REQUEST_VIEW_ALL);
        }
        catch (ActivityNotFoundException a) {
            a.printStackTrace();
        }
    }

    public void newPlace() {
        PlacebookEntry entry = new PlacebookEntry(items);
        entry.setName(placeContent.getText().toString());
        entry.setDescription(descriptionContent.getText().toString());
        entry.setPhotoPath(mCurrentPhotoPath);
        entry.setAltitude(AltitudeText.getText().toString());
        entry.setLongitude(LongitudeText.getText().toString());
        entry.setlatitude(LatitudeText.getText().toString());
        mPlaceBookEntries.add(entry);
    }

    public void changePlace(int newOne) {
        mPlaceBookEntries.get(newOne).setName(placeContent.getText().toString());
        mPlaceBookEntries.get(newOne).setDescription(descriptionContent.getText().toString());
        mPlaceBookEntries.get(newOne).setPhotoPath(mCurrentPhotoPath);
        mPlaceBookEntries.get(newOne).setAltitude(AltitudeText.getText().toString());
        mPlaceBookEntries.get(newOne).setLongitude(LongitudeText.getText().toString());
        mPlaceBookEntries.get(newOne).setlatitude(LatitudeText.getText().toString());
        change = -1;
    }

    @Override
    public void onLocationChanged(Location newlocation) {
        location = newlocation;
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }
}
