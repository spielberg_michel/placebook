package brendonsalinas.placebook;

import android.os.Parcel;
import android.os.Parcelable;

public class PlacebookEntry implements Parcelable {
    public final long id ;
    private String name ;
    private String description ;
    private String photoPath ;
    private String latitude;
    private String longitude;
    private String altitude;

    public PlacebookEntry(long num) {
        this.id = num;
    }

    //<editor-fold desc="SETTERS"
    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public void setlatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }
    //</editor-fold>

    //<editor-fold desc="GETTERS"
    public String getPhotoPath() {
        return photoPath;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public String getlatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getAltitude() {
        return altitude;
    }
    //</editor-fold>

    public PlacebookEntry ( Parcel source ) {
        this . id = source . readLong () ;
        this . name = source . readString () ;
        this . description = source . readString () ;
        this . photoPath = source . readString () ;
        //add more
    }

    @Override
    public void writeToParcel ( Parcel dest , int flags ) {
        dest . writeLong ( this . id );
        dest . writeString ( this . name );
        dest . writeString ( this . description );
        dest . writeString ( this . photoPath );
    }
    @Override
    public int describeContents () {
        return 0;
    }
    public static final Parcelable . Creator < PlacebookEntry > CREATOR
            = new Parcelable . Creator < PlacebookEntry >() {
        @Override
        public PlacebookEntry createFromParcel ( Parcel source ) {
            return new PlacebookEntry ( source );
        }
        @Override
        public PlacebookEntry [] newArray ( int size ) {
            return new PlacebookEntry [ size ];
        }
    };
}


